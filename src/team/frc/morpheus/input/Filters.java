package team.frc.morpheus.input;

import java.util.concurrent.Callable;

public class Filters {
	
	/**
	 * Generic "filter" interface to make working with various filters
	 * easier in the main loop.
	 * @author WCS
	 * @param <T> class on which filter will operate
	 */
	public interface IFilter<T> {
		public T getValue();
	}
	
	/**
	 * Make sure a value is no larger than 1 and no smaller than -1.
	 * @param value to be constrained
	 * @return constrained value
	 */
	public static double constrain(double value) {
		if (value > 1) return 1;
		if (value < -1) return -1;
		else return value;
	}
	
	/**
	 * Ensure neither Cartesian component of a {@link Coordinate} object is greater than 1 or
	 * less than -1. Coordinate version of {@link Filters#constrain(double)}.
	 * @param value to be constrained
	 * @return constrained coordinate pair
	 * @see Filters#constrain(double)
	 * @see Coordinate
	 */
	public static Coordinate constrain(Coordinate value) {
		return new Coordinate.Cartesian(constrain(value.x()), constrain(value.y()));
	}
	
	/**
	 * Make sure a value is greater than a specified threshold amount. If not, round down
	 * to zero.
	 * @param value to which threshold will be applied
	 * @param threshold above which values will not be rounded
	 * @return thresholded value
	 */
	public static double threshold(double value, double threshold) {
		if (Math.abs(value) < Math.abs(threshold)) return 0;
		else return value;
	}
	
	/**
	 * Ensure that neither Cartesian component of a {@link Coordinate} object is smaller
	 * than the supplied threshold value. Coordinate version of {@link #threshold(double, double)}.
	 * @param value to be thresholded
	 * @param threshold above which values will not be rounded to zero
	 * @return thresholded coordinate value
	 */
	public static Coordinate threshold(Coordinate value, double threshold) {
		return new Coordinate.Cartesian(threshold(value.x(), threshold), threshold(value.y(), threshold));
	}
	
	/**
	 * Perform both a threshold and constrain operation in one go. Useful for sanitizing
	 * joystick axis input, such as from #{@link Gamepad360#getLeftStickY()}.
	 * @param value to which the operations will be applied
	 * @param threshold for use in the threshold operation
	 * @return processed value
	 */
	public static double standard(double value, double threshold) {
		return constrain(threshold(value, threshold));
	}
	
	/**
	 * Pass a {@link Coordinate}'s Cartesian components through {@link #constrain(double)} and
	 * {@link #threshold(double, double)}. Useful for sanitizing 2D joystick input, such as
	 * from {@link Gamepad360#getLeftStick()}. Coordinate version of {@link #standard(double, double)}.
	 * @param value to which the operations will be applied
	 * @param threshold for use in the threshold operation
	 * @return processed value
	 */
	public static Coordinate standard(Coordinate value, double threshold) {
		return new Coordinate.Cartesian(standard(value.x(), threshold), standard(value.y(), threshold));
	}
	
	/**
	 * Filter class to persistently store (across loop iterations) the state of a variable
	 * in a manner similar to a standard flip-flop memory cell.
	 * @author WCS
	 * @see IFilter
	 */
	public static class FlipFlop implements IFilter<Boolean> {
		private Boolean value;
		Callable<Boolean> expression;
		public FlipFlop(Callable<Boolean> expression) {
			this.expression = expression;
		}
		public Boolean getValue() {
			/* I know the identical returns are redundant beyond measure,
			 * but there isn't much that can be done about it, so far as
			 * I'm aware.
			 */
			try {
				if (expression.call()) value = !value;
				return value;
			} catch (Exception e) { return value; }
		}
	}
	
	/**
	 * Filter class to track the state of a supplied expression and
	 * watch for a rising edge event. Useful for catching button presses.
	 * @author WCS
	 * @see IFilter
	 */
	public static class RisingEdge implements IFilter<Boolean> {
		private Boolean current, previous;
		Callable<Boolean> expression;
		public RisingEdge(Callable<Boolean> expression) {
			previous = current = false;
			this.expression = expression;
		}
		public Boolean getValue() {
			try {
				current = expression.call();
				boolean returnVal = !previous && current;
				previous = current;
				return returnVal;
			} catch (Throwable e) {
				return false;
			}
			/*try { return !previous && (current = expression.call()); }
			catch (Exception e) { return false; }
			finally { previous = current; }*/
		}
	}
	
	/**
	 * Filter class to track the state of a supplied expression and
	 * watch for a falling edge event. Useful for catching button releases.
	 * @author WCS
	 */
	public static class FallingEdge implements IFilter<Boolean> {
		private Boolean current, previous;
		Callable<Boolean> expression;
		public FallingEdge(Callable<Boolean> expression) {
			previous = current = false;
			this.expression = expression;
		}
		public Boolean getValue() {
			try {
				current = expression.call();
				boolean returnVal = previous && !current;
				previous = current;
				return returnVal;
			} catch (Throwable e) {
				return false;
			}
			/*try { return previous && !(current = expression.call()); }
			catch (Exception e) { return false; }
			finally { previous = current; }*/
		}
	}
}
