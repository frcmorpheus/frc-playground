package team.frc.morpheus.input;

import edu.wpi.first.wpilibj.Joystick;

/**
 * Joystick wrapper class providing convenience methods for
 * reading the state of buttons and axes on Xbox 360 controllers.
 */
public class Gamepad360 {
    public final Joystick hid;
    public Gamepad360(int port) {
        hid = new Joystick(port);
    }
    public void rumble(boolean toggle) {
      float actual = toggle ? 1.f : 0.f;
      hid.setRumble(Joystick.RumbleType.kLeftRumble, actual);
      hid.setRumble(Joystick.RumbleType.kRightRumble, actual);
    }

    public boolean getButtonA() { return hid.getRawButton(1); }
    public boolean getButtonB() { return hid.getRawButton(2); }
    public boolean getButtonX() { return hid.getRawButton(3); }
    public boolean getButtonY() { return hid.getRawButton(4); }
    public boolean getButtonLB() { return hid.getRawButton(5); }
    public boolean getButtonRB() { return hid.getRawButton(6); }
    public boolean getButtonBack() { return hid.getRawButton(7); }
    public boolean getButtonStart() { return hid.getRawButton(8); }
    public boolean getButtonLS() { return hid.getRawButton(9); }
    public boolean getButtonRS() { return hid.getRawButton(10); }
    public boolean getButtonUp() { return getPadY() < 0; }
    public boolean getButtonDown() { return getPadY() > 0; }
    public boolean getButtonLeft() { return getPadX() < 0; }
    public boolean getButtonRight() { return getPadX() > 0; }

    public double getLeftStickX() { return hid.getRawAxis(0); }
    public double getLeftStickY() { return -hid.getRawAxis(1); }
    public double getLeftTrigger() { return hid.getRawAxis(2); }
    public double getRightTrigger() { return hid.getRawAxis(3); }
    public double getRightStickX() { return hid.getRawAxis(4); }
    public double getRightStickY() { return -hid.getRawAxis(5); }
    public double getPadX() { return hid.getRawAxis(6); }
    public double getPadY() { return hid.getRawAxis(7); }
    public Coordinate getLeftStick() {
      return new Coordinate.Cartesian(getLeftStickX(), getLeftStickY());
    }
    public Coordinate getRightStick() {
      return new Coordinate.Cartesian(getRightStickX(), getRightStickY());
    }    
}
