package team.frc.morpheus.input;

import java.util.function.Function;

/**
 * Class which can represent Coordinates and/or Vectors. It can be either
 * Polar or Cartesian. Both forms are interoperable, but the speed of various
 * operations will vary depending on the underlying implementation.
 * @author WCS
 * @see Coordinate.Cartesian
 * @see Coordinate.Polar
 */
public abstract class Coordinate {

    /**
     * Return the current object in Cartesian form. If it is already in Cartesian
     * form the object returns itself.
     * @return the Coordinate in Cartesian form
     * @see Coordinate.Cartesian
     */
    public abstract Cartesian asCartesian();

    /**
     * Return the current object in Polar form. If it is already in Polar
     * form the object returns itself.
     * @return the Coordinate in Polar form
     * @see Coordinate.Polar
     */
    public abstract Polar asPolar();

    /**
     * Get the Theta component of a 2D coordinate. This method is more efficient
     * if the underlying implementation is Polar.
     * @return the Theta component of the current Coordinate
     * @see Coordinate.Polar
     */
    public double angle() { return asPolar().angle(); }

    /**
     * Get the Magnitude component of a 2D coordinate. This method is more efficient
     * if the underlying implementation is Polar.
     * @return the Magnitude component of the current Coordinate
     * @see Coordinate.Polar
     */
    public double magnitude() { return asPolar().magnitude(); }

    /**
     * Get the X component of a 2D coordinate. This method is more efficient
     * if the underlying implementation is Cartesian.
     * @return the X component of the current Coordinate
     * @see Coordinate.Cartesian
     */
    public double x() { return asCartesian().x(); }

    /**
     * Get the Y component of a 2D coordinate. This method is more efficient
     * if the underlying implementation is Cartesian.
     * @return the Y component of the current Coordinate
     * @see Coordinate.Cartesian
     */
    public double y() { return asCartesian().y(); }

    /**
     * Returns the sum of this and another Coordinate object. The underlying
     * implementation doesn't matter - it can be either Cartesian or Polar.
     * The resulting sum will be the same type as the current object.
     * @param other the Coordinate to add
     * @return sum of the two Coordinates
     */
    public abstract Coordinate add(Coordinate other);

    /**
     * Transform a Coordinate by applying an arbitrary filter
     * function to its components.
     * @param filter a filter function
     * @return transformed Coordinate
     */
    public Coordinate transform(Function<Coordinate, Coordinate> filter) { return filter.apply(this); }

    public static class Cartesian extends Coordinate {
        protected double x, y;
        @Override public double x() { return x; }
        @Override public double y() { return y; }
        public Cartesian(double x, double y) { this.x = x; this.y = y; }
        public Cartesian asCartesian() { return this; }
        public Coordinate add(Coordinate other) {
            return new Cartesian(x + other.x(), y + other.y());
        }
        public Polar asPolar() {
            double theta, shift, mag;
            theta = shift = mag = 0;
            mag = Math.sqrt(x*x + y*y);
            theta = Math.abs(Math.atan(y/x));
            if (x > 0) { /* upper and lower right quadrants */
                if (y > 0) shift = 0;
                if (y < 0) shift = 3;
            } else if (x < 0) { /* upper and lower left quadrants */
                if (y > 0) shift = 1;
                if (y < 0) shift = 2;
            }
            theta += (Math.PI / 2) * shift;
            return new Polar(theta, mag);
        }
    }

    public static class Polar extends Coordinate {
        protected double theta, mag;
        @Override public double angle() { return theta; }
        @Override public double magnitude() { return mag; }
        public Polar(double t, double m) { theta = t; mag = m; }
        public Polar asPolar() { return this; }
        public Coordinate add(Coordinate other) {
            return new Polar(theta + other.angle(), mag + other.magnitude());
        }
        public Cartesian asCartesian() {
            return new Cartesian(mag*Math.cos(theta),
                                 mag*Math.sin(theta));
        }

    }
}
