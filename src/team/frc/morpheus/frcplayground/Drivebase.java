package team.frc.morpheus.frcplayground;

import com.ctre.CANTalon;
import com.ctre.CANTalon.FeedbackDevice;
import com.ctre.CANTalon.TalonControlMode;

public class Drivebase {
	private CANTalon leftFront, rightFront, leftBack, rightBack;
	private int leftStartPos, rightStartPos;
	
	public Drivebase(CANTalon leftFront, CANTalon rightFront, CANTalon leftBack, CANTalon rightBack) {
		this.leftFront = leftFront;
		this.rightFront = rightFront;
		this.leftBack = leftBack;
		this.rightBack = rightBack;
		
		this.leftFront.setFeedbackDevice(FeedbackDevice.QuadEncoder);
		this.rightFront.setFeedbackDevice(FeedbackDevice.QuadEncoder);
		this.leftFront.configEncoderCodesPerRev(256);
		this.rightFront.configEncoderCodesPerRev(256);
		this.leftFront.reverseSensor(true);
		this.rightFront.reverseSensor(false);
		
		this.leftBack.changeControlMode(TalonControlMode.Follower);
		this.rightBack.changeControlMode(TalonControlMode.Follower);
		
		this.leftBack.set(leftFront.getDeviceID());
		this.rightBack.set(rightFront.getDeviceID());
		
		this.leftFront.setInverted(true);
		
		this.resetEncoders();
	}
	
	public void directSet(double left, double right) {
		this.leftFront.changeControlMode(TalonControlMode.PercentVbus);
		this.rightFront.changeControlMode(TalonControlMode.PercentVbus);
		
		this.leftFront.set(left);
		this.rightFront.set(right);
	}
	
	public int getLeftEncPosition() {
		return this.leftFront.getEncPosition() - this.leftStartPos;
	}
	
	public int getRightEncPosition() {
		return this.rightFront.getEncPosition() - this.rightStartPos;
	}
	
	public int getAveragePosition() {
		return (this.getLeftEncPosition() + this.getRightEncPosition()) / 2;
	}
	
	public void resetLeftEncPosition() {
		this.leftStartPos = this.leftFront.getEncPosition();
	}
	
	public void resetRightEncPosition() {
		this.rightStartPos = this.rightFront.getEncPosition();
	}
	
	public void resetEncoders() {
		this.resetLeftEncPosition();
		this.resetRightEncPosition();
	}

}
