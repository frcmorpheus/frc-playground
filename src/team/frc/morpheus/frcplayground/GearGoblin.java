package team.frc.morpheus.frcplayground;

import com.ctre.CANTalon;

import edu.wpi.first.wpilibj.Spark;

public class GearGoblin {
	public static final double INTAKE_IN = -1.0;
	public static final double INTAKE_OUT = 1.0;
	public static final double INTAKE_OFF = 0.0;
	
	public static final double POSITION_UP = 1.0;
	public static final double POSITION_DOWN = -1.0;
	public static final double POSITION_NEUTRAL = 0.0;
	
	private CANTalon intake;
	private Spark angler;
	
	public GearGoblin(CANTalon intake, Spark angler) {
		this.intake = intake;
		this.angler = angler;
		
		this.intake.changeControlMode(CANTalon.TalonControlMode.PercentVbus);
		this.intake.enableBrakeMode(false);
	}
	
	public void setIntake(double setVal) {
		this.intake.set(setVal);
	}
	
	public void setPosition(double setVal) {
		this.angler.set(setVal);
	}
	
	public double getIntakeCurrent() {
		return this.intake.getOutputCurrent();
	}
	
}
