package team.frc.morpheus.frcplayground;

import com.ctre.CANTalon;

import team.frc.morpheus.input.Filters;

public class Shooter {
	private CANTalon shooter, follower;
	
	public Shooter(CANTalon shooter, CANTalon follower) {
		this.shooter = shooter;
		this.follower = follower;
		
		this.shooter.setFeedbackDevice(CANTalon.FeedbackDevice.CtreMagEncoder_Relative);
		this.shooter.reverseOutput(true);
		this.shooter.reverseSensor(true);
		this.shooter.enableBrakeMode(false);
		this.shooter.setProfile(0); // configures PID profile
		this.shooter.configPeakOutputVoltage(12.0, -12.0);
		
		this.follower.changeControlMode(CANTalon.TalonControlMode.Follower);
		this.follower.set(this.shooter.getDeviceID());
	}
	
	public void setDirect(double throttle) {
		if(this.shooter.getControlMode() != CANTalon.TalonControlMode.PercentVbus) {
			this.shooter.changeControlMode(CANTalon.TalonControlMode.PercentVbus);
			this.shooter.enableControl();
		}
		this.shooter.set(Filters.constrain(throttle));
	}
	
	public void setSpeed(double rpm) {
		if(this.shooter.getControlMode() != CANTalon.TalonControlMode.Speed) {
			this.shooter.changeControlMode(CANTalon.TalonControlMode.Speed);
			this.shooter.enableControl();
		}
		this.shooter.set(rpm);
	}
	
	public double getSpeed() {
		return shooter.getEncVelocity() * 600 / 4096;
	}
}
