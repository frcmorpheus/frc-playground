package team.frc.morpheus.frcplayground;

import com.ctre.CANTalon;
import com.ctre.CANTalon.FeedbackDevice;
import com.ctre.CANTalon.TalonControlMode;

import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.Timer;
import team.frc.morpheus.input.Coordinate;
import team.frc.morpheus.input.Filters;
import team.frc.morpheus.input.Gamepad360;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {
	
	Drivebase drive;
	GearGoblin gearGoblin;
	Shooter shooter;
	Gamepad360 driver, operator;

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {
		drive = new Drivebase(new CANTalon(1), 
				new CANTalon(2), 
				new CANTalon(3), 
				new CANTalon(4));
		gearGoblin = new GearGoblin(new CANTalon(8), new Spark(4));
		shooter = new Shooter(new CANTalon(5), new CANTalon(7));
		driver = new Gamepad360(0);
		operator = new Gamepad360(1);
	}

	/**
	 * This method is called once at the beginning of autonomous
	 */
	@Override
	public void autonomousInit() {
		drive.directSet(0.5, 0.5);
		drive.resetEncoders();
		while(drive.getLeftEncPosition() < 1024 && Timer.getMatchTime() < 5.0) {
			if(!this.isAutonomous()) return;
		}
		drive.directSet(0, 0);
	}
	
	@Override
	public void disabledInit() {
		System.out.println("In disabledInit()");
		drive.directSet(0, 0);
	}

	/**
	 * This function is called periodically during autonomous
	 */
	@Override
	public void autonomousPeriodic() {
		
	}

	/**
	 * This function is called periodically during operator control
	 */
	@Override
	public void teleopPeriodic() {
		Coordinate stickVector = Filters.standard(driver.getLeftStick(), 0.15);
		drive.directSet(stickVector.y() + stickVector.x(),
						stickVector.y() - stickVector.x());
		
		if(driver.getButtonA()) {
			gearGoblin.setPosition(GearGoblin.POSITION_UP);
		} else if(driver.getButtonB()) {
			gearGoblin.setPosition(GearGoblin.POSITION_DOWN);
		}
		
		if(driver.getButtonX()) {
			gearGoblin.setIntake(GearGoblin.INTAKE_IN);
		} else if(driver.getButtonY()) {
			gearGoblin.setIntake(GearGoblin.INTAKE_OUT);
		} else {
			gearGoblin.setIntake(GearGoblin.INTAKE_OFF);
		}
		
		if(gearGoblin.getIntakeCurrent() > 30.0) {
			driver.rumble(true);
		} else {
			driver.rumble(false);
		}
		
		if(operator.getButtonA()) {
			shooter.setDirect(0.0);
		} else if(operator.getButtonB()) {
			shooter.setSpeed(3200.0);
		}
	}

	/**
	 * This function is called periodically during test mode
	 */
	@Override
	public void testPeriodic() {
		
	}
}

